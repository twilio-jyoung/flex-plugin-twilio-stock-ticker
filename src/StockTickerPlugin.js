import { FlexPlugin } from 'flex-plugin';
import React from 'react';
import { StockTickerComponent } from './StockTickerComponent';

export default class StockTickerPlugin extends FlexPlugin {
  name = 'StockTickerPlugin';

  init(flex, manager) {

	flex.MainHeader.Content.add(<StockTickerComponent key="stock-ticker-component" symbol="twlo"/>, {
      sortOrder: -1,
      align: "end"
    });
  }
}
