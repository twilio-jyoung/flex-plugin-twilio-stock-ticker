import React from 'react';

const panelStyle = {
	'margin': '10px',
	'background': 'white',
	'padding': '2px 2px 4px 4px',
	'border-radius': '5px',
	'color': 'black',
}

const realTimePrice = {
	'margin': ' 0 10px'
}

const trendUp = {
	'display': 'inline-block',
	'padding': '.4em .4em',
	'font-size': '.75em',
	'font-weight': '300',
	'line-height': '1',
	'text-align': 'center',
	'white-space': 'nowrap',
	'vertical-align': 'baseline',
	'border-radius': '.25rem',
	'color': 'white',
	'background-color': '#dc3545',
}

const trendDown = {
	'display': 'inline-block',
	'padding': '.4em .4em',
	'font-size': '.75em',
	'font-weight': '300',
	'line-height': '1',
	'text-align': 'center',
	'white-space': 'nowrap',
	'vertical-align': 'baseline',
	'border-radius': '.25rem',
	'color': 'white',
	'background-color': '#28a745',
}


export class StockTickerComponent extends React.Component {

	state = {
		stockData: null,
	}

	fetchStockData(){
		var url = "https://api.iextrading.com/1.0/stock/" + this.props.symbol + "/quote";

		fetch(url)
			.then(response => response.json())
		.then((results) => {
			this.setState({stockData: results})
		});
	}

	tick() {
		this.setState(prevState => ({
		 	seconds: prevState.seconds + 1
		}));
	}

	componentDidMount(){
		this.fetchStockData();

		this.timer = setInterval(()=> this.fetchStockData(), 60000);
	}

	componentWillUnmount() {
		clearInterval(this.timer);
		this.timer = null;
	}

	render(){
		const {stockData} = this.state;

		if (this.state && this.state.stockData){

			var trend = (this.state.stockData.change > 0) ? trendUp : trendDown;

			return (
				<div style={panelStyle}>
					<span>{this.state.stockData.symbol}</span>
					<span style={realTimePrice}>{this.state.stockData.iexRealtimePrice}</span>
					<span style={trendUp}>{this.state.stockData.change} ({this.state.stockData.changePercent}%)</span>
				</div>
			);

		}
		else
			return (<p>loading</p>);
	}
}


