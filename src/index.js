import * as FlexPlugin from 'flex-plugin';
import StockTickerPlugin from './StockTickerPlugin';

FlexPlugin.loadPlugin(StockTickerPlugin);
