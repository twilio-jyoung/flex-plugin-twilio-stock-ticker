## Twilio Flex - Twilio Stock Ticker

This plugin adds a stock ticker to the MainHeader component in Flex.  

This component calls https://api.iextrading.com/1.0/stock/twlo/quote for data every 60 seconds.  /src/StockTickerPlugin could be modified to pass a different stock symbol, or add multiple instances with unique stock symbols

![alt text](info/stock-ticker-sample.png)